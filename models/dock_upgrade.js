const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dock_upgradeSchema = new Schema({
    content: Array,
    remark: String,
    version: String,
    upgrade_count: Number,
    createTime: { type: Date, default: Date.now() }
})

dock_upgradeSchema.index({ version: 1 });

module.exports = mongoose.model("dock_upgrade", dock_upgradeSchema)