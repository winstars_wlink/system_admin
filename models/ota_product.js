var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ota_productSchema = new Schema({
    model: String,
    brand: String,
    start_mac: String,
    end_mac: String,
    last_update_time: { type: Date, default: Date.now() },
    created_time: { type: Date, default: Date.now() }
})

ota_productSchema.index({ model: 1 })

module.exports = mongoose.model("ota_product", ota_productSchema)