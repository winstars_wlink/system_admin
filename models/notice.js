var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notice = new Schema({
    service_code: String,
    uuid: String,
    priority: String,
    lang: String,
    title: String,
    content_type: String,
    contents: String,
    link_url: String,
    post_yn: String,
    start_date: Date,
    end_date: Date,
    created_date: { type: Date, default: Date.now }
}, { collection: 'notice' })

notice.index({ lang: 1 })
notice.index({ end_date: 1 }, { expireAfterSeconds: 60 })

module.exports = mongoose.model('notice', notice)