var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cloudClientStorageSchema = new Schema({
    service_code: String,
    client_key: String,
    uuid: String,
    key_name: String,
    key_value: String,
    shared: Number,
    shared_count: Number,
    shared_get_count: Number,
    shared_expired_time: Date,
    shared_key: String,
    shared_url: String,
}, { collection: 'cloudClientStorage' })

cloudClientStorageSchema.index({ service_code: 1, uuid: 1, client_key: 1, key_name: 1 })

module.exports = mongoose.model('cloudClientStorage', cloudClientStorageSchema)