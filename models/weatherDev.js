// models/weatherDev.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var weatherDevSchema = new Schema({
    brand: String,
    model: String, 
    id: String,   // mac address 

    // ip based info
    public_ip: String,
    ip_country_code: String, 
    ip_city_name: String, 
    ip_geo_lat: Number,
    ip_geo_lon: Number,

    // weather based info
    weather_city_id: String, 
    weather_city_name: String, 
    weather_country_code: String, 
    weather_lang: String, 
    weather_geo_lat: Number,
    weather_geo_lon: Number,
    weather_expired_time: Date,
    weather_now : {
      code: String,
      code2: String, // original weather code
      wind_scale: String,
      temperature: String,
      humidity: String,
      wind_direction: String,
      wind_direction2: String,  // original wind direction
      text: String
    },
    last_update_date : { type: Date, default: Date.now  },
    created_date : { type: Date, default: Date.now  },
}, { collection: 'weatherDev' });

weatherDevSchema.index({ mac: 1 });
weatherDevSchema.index({ brand: 1, model: 1, mac: 1 });
module.exports = mongoose.model('weatherDev', weatherDevSchema);