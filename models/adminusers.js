var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var adminusersSchema = new Schema({
    id: String,
    password: String,
    department: String,
    confirmed_id: Boolean,
    confirmed_date: Date,
    last_login_date: Date,
    login_count: { type: Number, default: 0 },
    login_token: String,
    login_expired: Date,
    created_date: { type: Date, default: Date.now }
}, { collection: 'adminusers' })

adminusersSchema.index({ id: 1 })

module.exports = mongoose.model('adminusers', adminusersSchema)