var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var appKeySchema = new Schema({
    key: String,
    is_multiple: { type: Boolean, default: false },
    is_autoreg: { type: Boolean, default: false },
}, { collection: 'appKey' })

appKeySchema.index({ key: 1 })

module.exports = mongoose.model('appKey', appKeySchema)

