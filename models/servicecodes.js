var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var servicecodesSchema = new Schema({
    service_name: String,
    service_code: String,
    service_tag: String,
    service_type: String,
    service_hashkey: String,
    expired_date: Date,
    session_expired_time: Number,
    last_update_date: { type: Date, default: Date.now },
    published_date: { type: Date, default: Date.now },
}, { collection: 'servicecodes' });

// servicecodesSchema.index({})

module.exports = mongoose.model('servicecodes', servicecodesSchema)