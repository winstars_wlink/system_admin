var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var devicesSchema = new Schema({
    service_code: String,
    uuid: String,
    expired_date: { type: Date, default: Date.now },
    active_count: Number,
    client_key: String,
    brand: String,
    model: String,
    sn: String,
    type: String,
    provision_date: Date,
    provision_key: String,
    provision_seed: String,
    provision_seed_device: String,
    provision_seed_fail_count: Number,
    provision_seed_fail_date: Date,
    created_time: { type: Date, default: Date.now },
    last_updated_date: { type: Date, default: Date.now },
}, { collection: 'devices' })

devicesSchema.index({ service_code: 1, uuid: 1 })

module.exports = mongoose.model('devices', devicesSchema)