const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ota_firmwareSchema = new Schema({
    model: String, 
    nickname: String,
    brand: String,
    priority: Number,
    type: String,
    version: String,
    released_date: String,
    start_mac: String,
    end_mac: String,
    firmware: String,
    description: String,
    update_history: String,
    last_update_time: { type: Date, default: Date.now() },
    created_time: { type: Date, default: Date.now() }
})

ota_firmwareSchema.index({ model: 1, priority: 1, version: 1 });

module.exports = mongoose.model('ota_firmware', ota_firmwareSchema);