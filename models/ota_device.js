const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ota_deviceSchema = new Schema({
    model: String,
    brand: String,
    mac: String,
    version: String,
    ip_address: String,
    last_priority: String,
    active_count: Number,
    last_update_time: { type: Date, default: Date.now() },
    created_time: { type: Date, default: Date.now() }
})
ota_deviceSchema.index({ model: 1, mac: 1, version: 1 })

module.exports = mongoose.model("ota_device", ota_deviceSchema)