var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var issue_categorySchema = new Schema({
    id: String,
    name: String,
    service_code: String,
    seo_search: String,
    describe: String,
    language: Number,
    status: { type: Number, default: 1 },
    created_date: { type: Date, default: Date.now }
}, { collection: 'issue_category' })

issue_categorySchema.index({ id: 1, name: 1, service_code: 1, seo_search: 1, language: 1 })

module.exports = mongoose.model('issue_category', issue_categorySchema)