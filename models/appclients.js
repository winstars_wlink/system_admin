var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appclientsSchema = new Schema({
    service_code: String,
    uuid: String,
    key: String,
    secret: String,
    session_key: String,
    is_multiple_use: { type: Boolean, default: false },
    is_admin: { type: Boolean, default: false },
    created_date: { type: Date, default: Date.now },
    last_inquired_date: { type: Date, default: Date.now }
}, { collection: "appclients" })

appclientsSchema.index({ service_code: 1, uuid: 1 }); // schema level

module.exports = mongoose.model('appclients', appclientsSchema)