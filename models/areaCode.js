var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var areaCodeSchema = new Schema({
    state: String,
    acronym: String,
    code: String,
    language: String,
    time_difference: String
}, { collection: 'areaCode' })

areaCodeSchema.index({ language: 1 })

module.exports = mongoose.model('areaCode', areaCodeSchema)

