var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var issue = new Schema({
    id: String,
    name: String,
    category_Id: String,
    service_code: String,
    problem: String,
    renderJSX: String,
    describe: String,
    language: Number,
    status: { type: Number, default: 1 },
    created_date: { type: Date, default: Date.now }
}, { collection: 'issue' })

issue.index({ id: 1, name: 1, category_Id: 1, service_code: 1, seo_search: 1, language: 1 })

module.exports = mongoose.model('issue', issue)