module.exports = {
  apps: [{
    // 应用程序名称
    name: 'admin_API', 
    // 应用程序目录，其他的路径配置如果是相对路径，则会相对此目录
    // cwd: '/home/chenbd/admin/service/source/bin/',
    // 启动脚本
    script: 'bin/www', 
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 'max',
    autorestart: true,
    watch: false,
    
    max_memory_restart: '1G',
    env_production: {
      NODE_ENV: 'production'
    },
  }],
  // 部署部份
  deploy: {
    production: {
      // 服务器用户名
      user: 'ubuntu',
      // 服务器 IP 地址
      host: 'wcloud-admin.wavlink.org',
      // git 分支
      ref: 'origin/master',
      // GIT 远程仓库地址
      repo: 'git@bitbucket.org:winstars_wlink/system_admin.git',
      // 应用程序在服务器上的部署路径
      path: '/home/ubuntu/service',
      // 安装前在服务器执行的命令，可以是 “;” 分割的多个命名，也可以是本地的脚本路径
      // 'pre-setup': 'apt-get install git',
      // 安装后在服务器执行的命令，也可以是服务器上脚本的路径
      // 'post-setup': 'sh /www/wlink/current/wlink_service/www.sh',
      // 部署前在本地执行的命令，可以是 “;” 分割的多个命名
      // 'pre-deploy-local': "echo 'hello word'",
      // 部署后在服务器执行的命令
      'post-deploy': 'cnpm install && pm2 reload ecosystem.config.js --env production',
      env: {
        NODE_ENV: "production"
      }
    }
  }
};
