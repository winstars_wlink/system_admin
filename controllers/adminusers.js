const adminuser = require('../src/adminusersImpl')
const jwt = require('jsonwebtoken');
const secret = "WLINK@#@GF"
// console.log(jwt.sign("wlink@123456", secret))
/**
 * wlink login 
 * @param {*} ctx 
 */
async function login(ctx) {
    var id = { id: ctx.query.id }
    var Password = ctx.query.password
    await adminuser.loginImpl(ctx, id, Password)
}

async function createAdminUser(ctx) {
    var insertStr = {
        id: "wlink@win-star.com",
        password: jwt.sign("wlink@123456", secret),
        department: "petiot",
        confirmed_id: true,
        confirmed_date: new Date,
        last_login_date: new Date
    }
    await adminuser.createAdminUserImpl(ctx, insertStr)
}

module.exports = {
    login,
    createAdminUser
}

