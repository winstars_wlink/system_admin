const { skips } = require('debug');
const mongoose = require('mongoose');
const devices = require('../models/devices');
var ota_device = require('../models/ota_device');
var ota_firmware = require('../models/ota_firmware')
var ota_product = require('../models/ota_product');

async function findByOTA_device(ctx) {
    try {
        if (ctx.query.skips == null || ctx.query.skips == "") {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" }
            return
        }
        if (ctx.query.limits == null || ctx.query.limits == "") {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" }
            return
        }
        var skips = ctx.query.skips;
        var limits = ctx.query.limits;
        var whereStr = {}
        if (ctx.query.model != '' && ctx.query.model != null) {
            whereStr.model = ctx.query.model;
        }
        if (ctx.query.version != '' && ctx.query.version != null) {
            whereStr.version = ctx.query.version;
        }
        if (ctx.query.mac != '' && ctx.query.mac != null) {
            whereStr.mac = ctx.query.mac;
        }
        var device_count = await ota_device.countDocuments(whereStr);
        console.log(device_count)
        var device = await ota_device.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits));
        ctx.status = 200;
        ctx.body = { count: device_count, device: device };
    } catch (err) {
        throw err;
    }
}

async function findByOTA_firmware(ctx) {
    try {
        if (ctx.query.skips == null || ctx.query.skips == "") {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" };
            return
        }
        if (ctx.query.limits == null || ctx.query.limits == "") {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" };
            return
        }
        var skips = ctx.query.skips;
        var limits = ctx.query.limits;
        var whereStr = {};
        if (ctx.query.model != "" && ctx.query.model != null) {
            whereStr.model = ctx.query.model;
        }
        if (ctx.query.nickname != "" && ctx.query.nickname != null) {
            whereStr.nickname = ctx.query.nickname;
        }
        if (ctx.query.brand != "" && ctx.query.brand != null) {
            whereStr.brand = ctx.query.brand;
        }
        if (ctx.query.priority != "" && ctx.query.priority != null) {
            whereStr.priority = ctx.query.priority;
        }
        if (ctx.query.type != "" && ctx.query.type != null) {
            whereStr.type = ctx.query.type;
        }
        if (ctx.query.version != "" && ctx.query.version != null) {
            whereStr.version = ctx.query.version;
        }
        if (ctx.query.start_mac != "" && ctx.query.start_mac != null) {
            whereStr.start_mac = ctx.query.start_mac;
        }
        if (ctx.query.end_mac != "" && ctx.query.end_mac != null) {
            whereStr.end_mac = ctx.query.end_mac;
        }
        var count = await ota_firmware.countDocuments(whereStr);
        var firmware = await ota_firmware.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits));
        ctx.status = 200;
        ctx.body = { count: count, firmware: firmware };
    } catch (error) {
        throw error;
    }
}

async function createOTA_firmware(ctx) {
    try {
        if (ctx.request.body.model == "" || ctx.request.body.model == null || ctx.request.body.priority == "" || ctx.request.body.priority == null ||
            ctx.request.body.type == "" || ctx.request.body.type == null || ctx.request.body.version == "" || ctx.request.body.version == null ||
            ctx.request.body.firmware == "" || ctx.request.body.firmware == null) {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" };
            return
        }
        var firmwares = {
            model: ctx.request.body.model,
            priority: ctx.request.body.priority,
            type: ctx.request.body.type,
            version: ctx.request.body.version,
            firmware: ctx.request.body.firmware,
        };
        firmwares.released_date = "2021.07.05";
        firmwares.update_history = ctx.request.body.version;
        if (ctx.request.body.nickname != "" && ctx.request.body.nickname != null) {
            firmwares.nickname = ctx.request.body.nickname;
        }
        if (ctx.request.body.brand != "" && ctx.request.body.brand != null) {
            firmwares.brand = ctx.request.body.brand;
        }
        if (ctx.request.body.start_mac != "" && ctx.request.body.start_mac != null) {
            firmwares.start_mac = ctx.request.body.start_mac;
        }
        if (ctx.request.body.end != "" && ctx.request.body.end_mac != null) {
            firmwares.end = ctx.request.body.end;
        }
        if (ctx.request.body.description != "" && ctx.request.body.description != null) {
            firmwares.description = ctx.request.body.description;
        }
        var db_firmware = new ota_firmware(firmwares);
        var ota_firmwares = await db_firmware.save(firmwares);
        if (ctx.errors != null) {
            ctx.status = 403;
            ctx.body = { message: "Save Failing" }
            return
        }
        ctx.status = 200;
        ctx.body = { message: "Save Success" };
    } catch (error) {
        throw error;
    }
}

async function delectOTA_firmware(ctx) {
    try {
        if (ctx.query.id == null || ctx.query.id == "") {
            ctx.status = 401;
            ctx.body = { message: "Delete ERROR" };
            return
        }
        var whereStr = { _id: ctx.query.id };
        var ota_firmwares = await ota_firmware.deleteOne(whereStr);
        console.log(ota_firmwares);
        if (ota_firmwares.n == 1 && ota_firmwares.ok == 1) {
            ctx.status = 200;
            ctx.body = { message: "Delete Success" };
        } else {
            ctx.body = { message: "Delete Failing" };
        }
    } catch (error) {
        throw error;
    }
}

async function updateOTA_firmware(ctx) {
    try {
        if (ctx.request.body._id == "" || ctx.request.body._id == null) {
            ctx.status = 401;
            ctx.body = { message: "Delete ERROR" };
            return
        }
        var whereStr = { _id: ctx.request.body._id };
        var ota_firmwares = await ota_firmware.updateOne(whereStr, ctx.request.body);
        if (ota_firmwares.n == 1 && ota_firmwares.ok == 1) {
            ctx.status = 200;
            ctx.body = { message: "Update Success" };
        } else {
            ctx.body = { message: "Update Failing" };
        }
    } catch (error) {
        throw error;
    }
}

module.exports = {
    findByOTA_device,
    findByOTA_firmware,
    createOTA_firmware,
    delectOTA_firmware,
    updateOTA_firmware
}