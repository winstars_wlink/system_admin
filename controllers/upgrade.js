const dock_upgrade = require('../models/dock_upgrade')

async function createByDock_Upgrade(ctx) {
    try {
        if (ctx.request.body.version == "" || ctx.request.body.version == null || ctx.request.body.content == null ||
            ctx.request.body.content.length == 0) {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" };
        }
        var upgrade_exist = await dock_upgrade.find({ version: ctx.request.body.version })
        console.log(upgrade_exist);
        if (upgrade_exist.length >= 1) {
            ctx.body = { message: "The version number already exists" }
            return
        }
        var db_upgrade = new dock_upgrade(ctx.request.body);
        var dock_upgrades = await db_upgrade.save(ctx.request.body);
        if (dock_upgrades.error != null) {
            ctx.status = 403;
            ctx.body = { message: "Save Failing" }
            return
        }
        ctx.status = 200;
        ctx.body = { message: "Save Success" };
    } catch (error) {
        throw error;
    }
}

async function findByDock_Upgrade(ctx) {
    try {
        if (ctx.query.skips == "" || ctx.query.skips == null || ctx.query.limits == "" || ctx.query.limits == null) {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" };
            return
        }
        var whereStr = {};
        if (ctx.query.version != "" && ctx.query.version != null) {
            whereStr.version = ctx.query.version;
        }
        var count = await dock_upgrade.countDocuments(whereStr);
        var dock_upgrades = await dock_upgrade.find(whereStr).skip(parseInt(ctx.query.skips)).limit(parseInt(ctx.query.limits));
        ctx.status = 200;
        ctx.body = { count: count, upgrade: dock_upgrades };
    } catch (error) {
        throw error;
    }
}

async function updateByDock_Upgrade(ctx) {
    try {
        if (ctx.request.body._id == "" || ctx.request.body._id == null) {
            ctx.status = 401;
            ctx.body = { message: "Delete ERROR" };
            return
        }
        var whereStr = { _id: ctx.request.body._id };
        var dock_upgrades = await dock_upgrade.updateOne(whereStr, ctx.request.body);
        if (dock_upgrades.n == 1 && dock_upgrades.ok == 1) {
            ctx.status = 200;
            ctx.body = { message: "Update Success" };
        } else {
            ctx.status = 404;
            ctx.body = { message: "Update Failing" };
        }
    } catch (error) {
        throw error;
    }
}

async function deleteByDock_Upgrade(ctx) {
    try {
        if (ctx.query.id == null || ctx.query.id == "") {
            ctx.status = 401;
            ctx.body = { message: "Invalid Requests" };
            return
        }
        var dock_upgrades = await dock_upgrade.deleteOne({ _id: ctx.query.id });
        if (dock_upgrades.n == 1 && dock_upgrades.ok == 1) {
            ctx.status = 200;
            ctx.body = { message: "Delete Success" };
        } else {
            ctx.status = 404;
            ctx.body = { message: "Delete Failing" };
        }
    } catch (error) {
        throw error;
    }
}

module.exports = {
    createByDock_Upgrade,
    findByDock_Upgrade,
    updateByDock_Upgrade,
    deleteByDock_Upgrade
}