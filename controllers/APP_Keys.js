const APP_KeysImpl = require('../src/APP_KeysImpl')

/**
 * find ALL by APP Keys 
 * @param {*} ctx 
 */
async function findByApp_Keys(ctx) {
    var skips = ctx.query.skips;
    var limits = ctx.query.limits;
    if (ctx.query.key != null && ctx.query.key != "") {
        var whereStr = {}
        whereStr.key = { $regex: ctx.query.key, $options: "i" }
        await APP_KeysImpl.findByApp_Keys_filterImpl(ctx, whereStr, skips, limits);
    } else {
        await APP_KeysImpl.findByApp_KeysImpl(ctx, skips, limits);
    }
}

/**
 * created APP_Keys 
 * @param {*} ctx 
 */
async function created_APP_Keys(ctx) {
    var insertStr = ctx.request.body;
    await APP_KeysImpl.created_APP_KeysImpl(ctx, insertStr)
}

/**
 * updated APP_Keys
 * @param {*} ctx 
 */
async function updated_APP_Keys(ctx) {
    var whereStr = { _id: ctx.request.body._id }
    var updateStr = ctx.request.body;
    await APP_KeysImpl.updated_APP_KeysImpl(ctx, whereStr, updateStr)
}

module.exports = {
    findByApp_Keys,
    created_APP_Keys,
    updated_APP_Keys
}