const devices = require('../models/devices')
const issue = require('../models/issue')
const issue_category = require('../models/issue_category');
const weatherDev = require('../models/weatherDev')
const Grid = require('gridfs-stream');
const mongoose = require('mongoose');

async function findByDeviceCount(ctx) {
    try {
        var res = { count: 0, on_line: 0 }
        var whereStr = {
            "last_updated_date": { $gte: new Date() }
        }
        // console.log(whereStr)
        var sum = await devices.countDocuments();
        // console.log(sum)
        res.count = sum;
        var on_sum = await devices.countDocuments(whereStr);
        // console.log(on_sum)
        res.on_line = on_sum;
        ctx.body = res;
    }
    catch (error) {
        ctx.status = 500
        ctx.body = { message: "Bad Request!" }
        throw error;
    }
}

async function findByOnlineDevice(ctx) {
    try {
        var whereStr = {
            "last_updated_date": { $gte: new Date() }
        }
        var skip = ctx.request.body.skip;
        var limit = ctx.request.body.limit;
        var count = await devices.countDocuments(whereStr);
        var device = await devices.find(whereStr).skip(skip).limit(limit);
        // console.log(device)
        if (device) {
            var res = { count: 0, device: [] }
            res.count = count;
            res.device = device
            ctx.status = 200;
            ctx.body = res;
        } else {
            ctx.status = 401;
            ctx.body = { message: "无设备列表" };
        }
    } catch (error) {
        ctx.status = 500
        ctx.body = { message: "Bad Request!" }
        throw error;
    }
}

async function findByOfflineDevice(ctx) {
    try {
        var whereStr = {
            "last_updated_date": { $lte: new Date() }
        }
        var skip = ctx.request.body.skip;
        var limit = ctx.request.body.limit;
        var count = await devices.countDocuments(whereStr);
        var device = await devices.find(whereStr).skip(skip).limit(limit);
        if (device) {
            var res = { count: 0, device: [] }
            res.count = count;
            res.device = device
            ctx.status = 200;
            ctx.body = res;
        } else {
            ctx.status = 401;
            ctx.body = { message: "无设备列表" };
        }
    } catch (error) {
        ctx.status = 500
        ctx.body = { message: "Bad Request!" }
        throw error;
    }
}

async function findByIssueCount(ctx) {
    try {
        var res = { issue_count: 0, issue_category_count: 0 }
        var issue_count = await issue.countDocuments({ status: 1 });
        var issue_category_count = await issue_category.countDocuments({ status: 1 });
        if (issue_count > 0) {
            res.issue_count = issue_count;
        }
        if (issue_category_count > 0) {
            res.issue_category_count = issue_category_count;
        }
        ctx.status = 200;
        ctx.body = res;
    } catch (error) {
        ctx.status = 500
        ctx.body = { message: "Bad Request!" }
        throw error;
    }
}

async function findByDeviceWorldDistribution(ctx) {
    try {
        var whereStr = [
            {
                $group: {
                    _id: '$weather_country_code',
                    counts: { $sum: 1 },
                }
            }
        ]
        var weatherdev = await weatherDev.aggregate(whereStr);
        // console.log(weatherdev);
        ctx.status = 200;
        ctx.body = weatherdev;
    } catch (error) {
        ctx.status = 500;
        ctx.body = { message: "Bad Request!" }
        throw error;
    }
}

async function findByFSCount(ctx) {
    try {
        var conn = mongoose.connection;

        var gfs = Grid(conn.db, 'wlink');
        var whereStr = [
            {
                $group: {
                    _id: null,
                    lengths: { $sum: "$length" },
                }
            }
        ]
        var FileCount = await gfs.files.countDocuments();
        // console.log(FileCount)
        var FileSize = await gfs.files.aggregate(whereStr).toArray()
        // var FileSize = await gfs.files.find().toArray()
        // console.log(FileSize)
        var res = { FileCount: 0, FileSize: 0 }
        if (FileCount > 0) {
            res.FileCount = FileCount;
        }
        if (FileSize) {
            res.FileSize = FileSize[0].lengths;
        }
        ctx.status = 200;
        ctx.body = res;
    } catch (error) {
        ctx.status = 500;
        ctx.body = { message: "Bad Request!" };
        throw error;
    }
}

async function findByFSList(ctx) {
    try {
        var skip = ctx.request.body.skip;
        var limit = ctx.request.body.limit;
        var conn = mongoose.connection;
        var gfs = Grid(conn.db, 'wlink');
        var FileCount = await gfs.files.countDocuments();
        var FSLIST = await gfs.files.find().skip(parseInt(skip)).limit(parseInt(limit)).toArray();
        var res = { FileCount: 0, FSLIST: [] }
        if (FileCount > 0) {
            res.FileCount = FileCount;
        }
        if (FSLIST.length > 0) {
            res.FSLIST = FSLIST;
        }
        ctx.status = 200;
        ctx.body = res;
    } catch (error) {
        ctx.status = 500;
        ctx.body = { message: "Bad Request!" };
        throw error;
    }
}

module.exports = {
    findByDeviceCount,
    findByOfflineDevice,
    findByOnlineDevice,
    findByIssueCount,
    findByDeviceWorldDistribution,
    findByFSCount,
    findByFSList
}