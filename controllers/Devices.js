const DevicesImpl = require('../src/DevicesImpl');


/**
 * find ALL by devices
 * @param {*} ctx 
 */
async function findbyDevices(ctx) {
    var skips = ctx.query.skips;
    var limits = ctx.query.limits;
    if ((ctx.query.uuid != null && ctx.query.uuid != "") || (ctx.query.brand != null && ctx.query.brand != "") || (ctx.query.device_name != null && ctx.query.device_name != "") || (ctx.query.created_time != null && ctx.query.created_time != "") || (ctx.query.last_updated_date != null && ctx.query.last_updated_date != "")) {
        var whereStr = {}
        if (ctx.query.uuid != "") {
            whereStr.uuid = { $regex: ctx.query.uuid, $options: "i" }
        }
        if (ctx.query.brand != "") {
            whereStr.brand = { $regex: ctx.query.brand, $options: "i" }
        }
        if (ctx.query.device_name != "") {
            whereStr["push_clients.name"] = { $regex: ctx.query.device_name, $options: "i" }
        }
        if (ctx.query.created_time != "") {
            whereStr.created_time = { $gt: ctx.query.created_time }
        }
        if (ctx.query.last_updated_date != "") {
            whereStr.last_updated_date = { $gt: ctx.query.last_updated_date }
        }
        console.log(whereStr)
        await DevicesImpl.findbyDevices_filterImpl(ctx, whereStr, skips, limits)
    } else {
        await DevicesImpl.findbyDevicesImpl(ctx, skips, limits)
    }
}

/**
 * created Devices
 * @param {*} ctx 
 */
async function created_Devices(ctx) {
    var insertStr = ctx.request.body;
    await DevicesImpl.created_DevicesImpl(ctx, insertStr)
}

/**
 * updated Devices
 * @param {*} ctx 
 */
async function updated_Devices(ctx) {
    var whereStr = { _id: ctx.request.body._id };
    var updateStr = ctx.request.body;
    await DevicesImpl.updated_DevicesImpl(ctx, whereStr, updateStr)
}

module.exports = {
    findbyDevices,
    created_Devices,
    updated_Devices
}