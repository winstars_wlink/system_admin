const users = require('../models/users');
const mongose = require('mongoose');
async function findByUsers(ctx) {
    try {
        var skip = ctx.query.skip;
        var limit = ctx.query.limit;
        var res = { count: 0, users: [] }
        var count = await users.countDocuments();
        if (count > 0) {
            res.count = count;
        }
        var whereStr = [
            { "$addFields": { "_id": { "$toString": "$_id" } } },
            {
                $lookup: {
                    from: "userDevices",
                    localField: "_id",
                    foreignField: "user_key",
                    as: "userDevices"
                }
            },
            {
                $skip: parseInt(skip)
            },
            {
                $limit: parseInt(limit)
            }
        ]
        if ((ctx.query.account_type != null && ctx.query.account_type != "") || (ctx.query.account_id != null && ctx.query.account_id != "") || (ctx.query.mobile != null && ctx.query.mobile != "") || (ctx.query.email != null && ctx.query.email != "") || (ctx.query.device_name != null && ctx.query.device_name != "") || (ctx.query.device_uuid != null && ctx.query.device_uuid != "")) {
            if (ctx.query.account_type != null && ctx.query.account_type != "") {
                whereStr.push({
                    $match: {
                        "account_type": ctx.query.account_type
                    }
                })
            }
            if (ctx.query.account_id != null && ctx.query.account_id != "") {
                if (whereStr[whereStr.length - 1]['$match'] != null) {
                    whereStr[whereStr.length - 1]['$match'].account_id = ctx.query.account_id
                } else {
                    whereStr.push({
                        $match: {
                            "account_id": ctx.query.account_id
                        }
                    })
                }
            }
            if (ctx.query.mobile != null && ctx.query.mobile != "") {
                if (whereStr[whereStr.length - 1]['$match'] != null) {
                    whereStr[whereStr.length - 1]['$match'].mobile = ctx.query.mobile
                } else {
                    whereStr.push({
                        $match: {
                            "mobile": ctx.query.mobile
                        }
                    })
                }
            }
            if (ctx.query.email != null && ctx.query.email != "") {
                if (whereStr[whereStr.length - 1]['$match'] != null) {
                    whereStr[whereStr.length - 1]['$match'].email = ctx.query.email
                } else {
                    whereStr.push({
                        $match: {
                            "email": ctx.query.email
                        }
                    })
                }
            }
            if (ctx.query.device_name != null && ctx.query.device_name != "") {
                if (whereStr[whereStr.length - 1]['$match'] != null) {
                    whereStr[whereStr.length - 1]['$match']['userDevices.device_name'] = ctx.query.device_name
                } else {
                    whereStr.push({
                        $match: {
                            "userDevices.device_name": ctx.query.device_name
                        }
                    })
                }
            }
            if (ctx.query.device_uuid != null && ctx.query.device_uuid != "") {
                if (whereStr[whereStr.length - 1]['$match'] != null) {
                    whereStr[whereStr.length - 1]['$match']['userDevices.device_uuid'] = ctx.query.device_uuid
                } else {
                    whereStr.push({
                        $match: {
                            "userDevices.device_uuid": ctx.query.device_uuid
                        }
                    })
                }
            }
        }
        // var user = await users.find().skip(parseInt(skip)).limit(parseInt(limit));
        var user = await users.aggregate(whereStr)
        if (user.length > 0) {
            res.users = user;
        }
        ctx.status = 200;
        ctx.body = res;
    } catch (error) {
        ctx.status = 500;
        ctx.body = { message: "System Error" }
        throw error;
    }
}

function generateDate(Str) {
    if (Str == "upDate_gte") {
        var Year = new Date().getFullYear();
        var Month = new Date().getMonth();
        var Day = "01"
        if (Month == 0) {
            Year = new Date().getFullYear();
            Month = 12
        }
        return Year + "-" + (Month <= 9 ? '0' + Month : Month) + "-" +  Day + " 00:00:00"
    }
    if (Str == "upDate_lte") {
        var Year = new Date().getFullYear();
        var Month = new Date().getMonth() + 1;
        var Day = "01"
        // console.log(Year + "-" + (Month <= 9 ? '0' + Month : Month) + "-" + (Day <= 9 ? '0' + Day : Day) + " 00:00:00")
        return Year + "-" + (Month <= 9 ? '0' + Month : Month) + "-" + Day + " 00:00:00"
    }
    if (Str == "thisDate_gte") {
        var Year = new Date().getFullYear();
        var Month = new Date().getMonth() + 1;
        var Day = "01"
        return Year + "-" + (Month <= 9 ? '0' + Month : Month) + "-" + Day+ " 00:00:00"
    }
    if (Str == "thisDate_lte") {
        var Year = new Date().getFullYear();
        var Month = new Date().getMonth() + 1;
        var Day = "01"
        if (Month == 12) {
            Year = new Date().getFullYear() + 1;
            Month = 1
        }
        return Year + "-" + (Month <= 9 ? '0' + Month : Month) + "-" + Day + " 00:00:00"
    }
}

const moment = require('moment-timezone')
async function findByUsersChart(ctx) {
    try {
        var whereStr1 = {
            created_time: { $gte: new Date(generateDate('upDate_gte')), $lte: new Date(generateDate('upDate_lte')) }
        }
        var whereStr2 = {
            created_time: { $gte: new Date(generateDate('thisDate_gte')), $lte: new Date(generateDate('thisDate_lte')) }
        }
        var sumCount = await users.countDocuments();
        var upCount = await users.countDocuments(whereStr1);
        var thisCount = await users.countDocuments(whereStr2);
        var res = { sumCount: 0, upCount: 0, thisCount: 0 };
        if (sumCount > 0) {
            res.sumCount = sumCount
        }
        if (upCount > 0) {
            res.upCount = upCount
        }
        if (thisCount > 0) {
            res.thisCount = thisCount
        }
        ctx.status = 200;
        ctx.body = res;
    } catch (error) {
        ctx.status = 500;
        ctx.body = { message: "System ERROR" };
        throw error;
    }
}


module.exports = {
    findByUsers,
    findByUsersChart
}