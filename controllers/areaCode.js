const areaCode = require('../models/areaCode');



async function findByareaCode(ctx) {
    try {
        var skip = ctx.query.skip;
        var limit = ctx.query.limit;
        var whereStr = {};
        if (ctx.query.language != null && ctx.query.language != "") {
            whereStr.language = ctx.query.language;
        }
        if (ctx.query.state != null && ctx.query.state != "") {
            whereStr.state = { $regex: ctx.query.state };
        }
        if (ctx.query.code != null && ctx.query.code != "") {
            whereStr.code = { $regex: ctx.query.code };
        }
        var counts = await areaCode.countDocuments(whereStr);
        var areaCodes = await areaCode.find(whereStr).skip(parseInt(skip)).limit(parseInt(limit));
        var RES = { count: 0, areaCode: [] }
        if (counts > 0) {
            RES.count = counts;
        }
        if (areaCodes) {
            RES.areaCode = areaCodes;
        }
        ctx.status = 200;
        ctx.body = RES;
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function deleteByareaCodeId(ctx) {
    try {
        if (ctx.query.id == null || ctx.query.id == "") {
            ctx.body = { message: "Delete ERROR" };
            return
        }
        var whereStr = { _id: ctx.query.id };
        var area = await areaCode.deleteOne(whereStr);
        // console.log(area);
        if (area.n == 1 && area.ok == 1) {
            ctx.status = 200;
            ctx.body = { message: "DELETE Success" }
        } else {
            ctx.body = { message: "DELETE ERROR" };
        }
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function createAreaCode(ctx) {
    try {
        var areas = ctx.request.body;
        console.log(areas);
        var areaCodes = new areaCode(areas);
        var area = await areaCodes.save(areas);
        console.log(area);
        ctx.status = 200;
        ctx.body = { message: "Save Success" };
    } catch (error) {
        console.log(error);
        throw error;
    }
}

module.exports = {
    findByareaCode,
    deleteByareaCodeId,
    createAreaCode
}