const issue_categorys = require('../models/issue_category')

/**
 * 获取N位随机数
 * @param {} n 
 */
function randomn(n) {
    if (n > 21) return null
    return parseInt((Math.random() + 1) * Math.pow(10, n - 1))
}

function pad2(n) { return n < 10 ? '0' + n : n }

/**
 * 随机数 加上 时间戳
 */
function generateProductId() {
    var date = new Date();
    var time = date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours()) + pad2(date.getMinutes()) + pad2(date.getSeconds())
    var num = randomn(4)
    return num + "" + time
}

async function create_issue_category(ctx) {
    var insertStr = ctx.request.body;
    console.log(insertStr)
    insertStr.id = generateProductId();
    var issue_category = new issue_categorys(insertStr);
    await issue_category.save(insertStr).then(res => {
        if (res) {
            ctx.status = 200
            ctx.body = res
        } else {
            ctx.status = 404
            ctx.body = { message: "Save fail !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "Save error !" }
        throw error;
    })
}

async function findBy_issue_category(ctx) {
    var skips = ctx.query.skips;
    var limits = ctx.query.limits;
    var json = {}
    await issue_categorys.countDocuments({ status: 1 }).then(res1 => {
        console.log(res1)
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.status = 404
            ctx.body = { message: "not data" }
            return
        }
    })
    await issue_categorys.find({ status: 1 }).skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        if (res2) {
            json.datas = res2
            ctx.status = 200
            ctx.body = json
        } else {
            ctx.status = 404
            ctx.body = { message: "find fail !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "Save error !" }
        throw error;
    })
}

async function findBy_issue_category_options(ctx) {
    var whereStr = { status: 1, }
    if (ctx.query.language != "" && ctx.query.language != null) {
        whereStr.language = parseInt(ctx.query.language)
    }
    if (ctx.query.service_code != "" && ctx.query.service_code != null) {
        whereStr.service_code = ctx.query.service_code
    }
    await issue_categorys.find(whereStr).then(res => {
        if (res) {
            ctx.status = 200
            ctx.body = res
        } else {
            ctx.status = 404
            ctx.body = { message: "find fail !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "Save error !" }
        throw error;
    })
}

async function findBy_issue_category_service_code(ctx) {
    await issue_categorys.find({ status: 1 }).distinct("service_code").then(res => {
        if (res) {
            ctx.status = 200
            ctx.body = res
        } else {
            ctx.status = 404
            ctx.body = { message: "find fail !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "Save error !" }
        throw error;
    })
}

async function delete_issue_category(ctx) {
    var whereStr = { id: ctx.query.id }
    var updateStr = { status: 2 }
    await issue_categorys.updateOne(whereStr, updateStr).then(res => {
        if (res.n == 1 && res.ok == 1) {
            ctx.status = 200
            ctx.body = { message: "Delete Success" }
        } else {
            ctx.status = 404
            ctx.body = { message: "Delete Fail" }
        }
    }).catch(error => {
        ctx.status = 500
        ctx.body = { message: "Delete Fail" }
        throw error;
    })
}

module.exports = {
    create_issue_category,
    findBy_issue_category,
    findBy_issue_category_options,
    findBy_issue_category_service_code,
    delete_issue_category
}