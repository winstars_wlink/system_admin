const service_CodesImpl = require("../src/Service_CodesImpl")

/**
 * find by Byservice_Codes Tables params {service_name,service_code}
 * 
 * find All Byservice_Codes
 * @param {*} ctx 
 */
async function findByservice_Codes(ctx) {
    var skips = ctx.query.skips;
    var limits = ctx.query.limits;
    // Determine search parameters Execute the corresponding funciton 
    if ((ctx.query.service_name != null && ctx.query.service_name != "") || (ctx.query.service_code != null && ctx.query.service_code != "")) {
        var whereStr = {}
        if (ctx.query.service_name != "") {
            whereStr.service_name = { $regex: ctx.query.service_name, $options: "i" }
        } else if (ctx.query.service_code != "") {
            whereStr.service_code = { $regex: ctx.query.service_code, $options: "i" }
        }
        // find by Byservice_Codes Tables params {service_name,service_code}
        await service_CodesImpl.findByservice_Codes_filterImpl(ctx, whereStr, skips, limits)
    } else {
        // find All Byservice_Codes
        await service_CodesImpl.findByservice_CodesImpl(ctx, skips, limits)
    }
}

/**
 * find by Byservice_Codes Tables params {service_name,service_code}
 * @param {*} ctx 
 */
// async function findByservice_Codes_filter(ctx) {
//     var whereStr = {}
//     if (ctx.request.body.service_name != "") {
//         whereStr.service_name = { $regex: ctx.request.body.service_name }
//     } else if (ctx.request.body.service_code != "") {
//         whereStr.service_code = { $regex: ctx.request.body.service_code }
//     }
//     var skips = ctx.query.skips;
//     var limits = ctx.query.limits;
//     await service_CodesImpl.findByservice_Codes_filterImpl(ctx, whereStr, skips, limits)
// }

//cteated service_Codes
async function created_service_Codes(ctx) {
    var insertStr = ctx.request.body;
    insertStr.expired_date = new Date(insertStr.expired_date)
    insertStr.session_expired_time = 360
    console.log(insertStr);
    await service_CodesImpl.created_service_CodesImpl(ctx, insertStr);
}

/**
 * updated service_Codes 
 * @param {*} ctx 
 */
async function updated_service_Codes(ctx) {
    var whereStr = { _id: ctx.request.body._id };
    var updateStr = ctx.request.body;
    updateStr.expired_date = new Date(updateStr.expired_date)
    await service_CodesImpl.updated_service_CodesImpl(ctx, whereStr, updateStr);
}

async function findByservice_code(ctx) {
    await service_CodesImpl.findByservice_codeImpl(ctx)
}

module.exports = {
    findByservice_Codes,
    // findByservice_Codes_filter,
    created_service_Codes,
    updated_service_Codes,
    findByservice_code
}