const db_notice = require('../models/notice');


async function createNotice(ctx) {
    try {
        var notice = ctx.request.body;
        notice.end_date = notice.end_date != "" ? notice.end_date + " 23:59:59.000" : "";
        var db_notices = new db_notice(notice);
        var Save_status = await db_notices.save(notice);
        if (Save_status) {
            ctx.status = 200;
            ctx.body = { "message": "Success" }
        } else {
            ctx.status = 403;
            ctx.body = { "message": "New Notice Failing" }
        }
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function findByNotices(ctx) {
    try {
        var skip = ctx.request.body.skip;
        var limit = ctx.request.body.limit;
        var whereStr = {};
        if (ctx.request.body.lang != null && ctx.request.body.lang != "") {
            whereStr.lang = ctx.request.body.lang;
        }
        if (ctx.request.body.service_code != null && ctx.request.body.service_code != "") {
            whereStr.service_code = ctx.request.body.service_code;
        }
        if (ctx.request.body.content_type != null && ctx.request.body.content_type != "") {
            whereStr.content_type = ctx.request.body.content_type;
        }
        if (ctx.request.body.post_yn != null && ctx.request.body.post_yn != "") {
            whereStr.post_yn = ctx.request.body.post_yn;
        }
        if (ctx.request.body.time_off_no != null && ctx.request.body.time_off_no != "") {
            if (ctx.request.body.time_off_no == "1") {
                whereStr.end_date = { $gte: new Date() };
            } else if (ctx.request.body.time_off_no == "0") {
                whereStr.end_date = { $lte: new Date() };
            }
        }
        if (ctx.request.body.title != null && ctx.request.body.title != "") {
            whereStr.title = { $regex: ctx.request.body.title };
        }
        console.log(whereStr)
        var count = await db_notice.countDocuments(whereStr);
        var Notice_Arr = await db_notice.find(whereStr).skip(parseInt(skip)).limit(parseInt(limit));
        var res = { count: 0, notices: [] }
        if (count > 0) {
            res.count = count;
        }
        if (Notice_Arr.length > 0) {
            res.notices = Notice_Arr;
        }
        ctx.status = 200;
        ctx.body = res;
    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function deleteByNotice(ctx) {
    try {
        var _id = ctx.request.body._id;
        var notice = await db_notice.deleteOne({ _id: _id });
        if (notice.n == 1 && notice.ok == 1) {
            ctx.status = 200;
            ctx.body = { message: "Success" };
        } else {
            ctx.status = 402;
            ctx.body = { message: "Failing" };
        }

    } catch (error) {
        console.log(error);
        throw error;
    }
}

async function notice_post(ctx) {
    console.log(ctx)
    try {
        var service_code = ctx.request.body.service_code;
        var command = ctx.request.body.command;
        if (service_code == null || service_code == "") {
            ctx.status = 400;
            ctx.body = { message: "Bad Request, SERVICE_CODE Failing" }
            return
        }
        if (command == null || command == "") {
            ctx.status = 400;
            ctx.body = { message: "Bad Request, COMMAND Failing" }
            return
        }
        switch (command) {
            case "new":
                await createNotice(ctx);
                break;
            case "get":
                await findByNotices(ctx);
                break;
            case "delete":
                await deleteByNotice(ctx);
                break;
            case "update":
                await updateByNotice(ctx);
                break;
            default:
                ctx.status = 401;
                ctx.body = { message: "Bad Request. ERROR COMMAND" }
                break;
        }
    } catch (error) {
        console.log(error);
        throw error;
    }
}



module.exports = {
    notice_post
}
