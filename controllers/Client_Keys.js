const client_keysImpl = require('../src/Client_KeysImpl')

/**
 * find ALL By appclients （paging）
 * @param {*} ctx 
 */
async function findByClient_Keys(ctx) {
    var skips = ctx.query.skips;
    var limits = ctx.query.limits;
    if ((ctx.query.uuid != null && ctx.query.uuid != "") || (ctx.query.key != null && ctx.query.key != "") || (ctx.query.created_date != null && ctx.query.created_date != "") || (ctx.query.last_inquired_date != null && ctx.query.last_inquired_date != "")) {
        console.log((ctx.query.uuid !== null && ctx.query.uuid != "") || (ctx.query.key != null && ctx.query.key != "") || (ctx.query.created_date != null && ctx.query.created_date != "") || (ctx.query.last_inquired_date != null && ctx.query.last_inquired_date != ""))
        var whereStr = {}
        if (ctx.query.uuid != "") {
            whereStr.uuid = { $regex: ctx.query.uuid, $options: "i" }
        }
        if (ctx.query.key != "") {
            whereStr.key = { $regex: ctx.query.key, $options: "i" }
        }
        if (ctx.query.created_date != "") {
            whereStr.created_date = { $gt: ctx.query.created_date }
        }
        if (ctx.query.last_inquired_date != "") {
            whereStr.last_inquired_date = { $gt: ctx.query.last_inquired_date }
        }
        console.log(whereStr)
        await client_keysImpl.findByClient_Keys_filterImpl(ctx, whereStr, skips, limits);
    } else {
        await client_keysImpl.findByClient_KeysImpl(ctx, skips, limits)
    }
}

/**
 * find By Service_Codes 
 * create appclients selete Service_code 
 * @param {*} ctx 
 */
async function findByservice_Codes(ctx) {
    await client_keysImpl.findByservice_CodesImpl(ctx);
}

/**
 * created appclients
 * @param {*} ctx 
 */
async function created_client_Keys(ctx) {
    var insertStr = ctx.request.body;
    await client_keysImpl.created_client_KeysImpl(ctx, insertStr)
}

/**
 * updated appclients
 * @param {*} ctx 
 */
async function updated_client_Keys(ctx) {
    var whereStr = { _id: ctx.request.body._id };
    var updateStr = ctx.request.body;
    await client_keysImpl.updated_client_KeysImpl(ctx, whereStr, updateStr)
}

module.exports = {
    findByClient_Keys,
    findByservice_Codes,
    created_client_Keys,
    updated_client_Keys
}