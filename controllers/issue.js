var issue = require('../models/issue')
var issue_category = require('../models/issue_category')

/**
 * 获取N位随机数
 * @param {} n 
 */
function randomn(n) {
    if (n > 21) return null
    return parseInt((Math.random() + 1) * Math.pow(10, n - 1))
}

function pad2(n) { return n < 10 ? '0' + n : n }

/**
 * 随机数 加上 时间戳
 */
function generateProductId() {
    var date = new Date();
    var time = date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours()) + pad2(date.getMinutes()) + pad2(date.getSeconds())
    var num = randomn(4)
    return num + "" + time
}

async function Save(ctx) {
    var insertStr = ctx.request.body;
    insertStr.id = generateProductId();
    insertStr.problem = insertStr.describe
    var issues = new issue(insertStr);
    await issues.save(insertStr).then(res => {
        if (res) {
            ctx.status = 200
            ctx.body = { message: "Save Success" }
        } else {
            ctx.status = 404
            ctx.body = { message: "Save fail" }
        }
    }).catch(error => {
        ctx.status = 500;
        ctx.body = { message: error };
        throw error;
    })
}

async function Find(ctx) {
    var skips = ctx.query.skips;
    var limits = ctx.query.limits;
    var json = {}
    var whereStr = { status: 1 }
    var sortStr = {}
    if (ctx.query.category_Id != "" && ctx.query.category_Id != null) {
        if (ctx.query.category_Id != 1) {
            whereStr.category_Id = ctx.query.category_Id
        }
    }
    if (ctx.query.service_code != "" && ctx.query.service_code != null) {
        whereStr.service_code = ctx.query.service_code
    }
    if (ctx.query.language != "" && ctx.query.language != null) {
        whereStr.language = parseInt(ctx.query.language)
    }
    if (ctx.query.sort != "" && ctx.query.sort != null) {
        sortStr.created_date = ctx.query.sort == 1 ? 1 : -1
    }
    await issue.countDocuments(whereStr).then(res1 => {
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.status = 404
            ctx.body = { message: "Not data" }
            return
        }
    })
    await issue.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits)).sort(sortStr).then(res2 => {
        if (res2) {
            json.datas = res2
            ctx.status = 200
            ctx.body = json
        } else {
            ctx.status = 404
            ctx.body = { message: "Not data" }
        }
    }).catch(error => {
        ctx.status = 500;
        ctx.body = { message: error };
        throw error;
    })
}

async function findBycategory(ctx) {
    var whereStr = {
        status: 1,
        service_code: ctx.query.service_code,
        language: ctx.query.language
    }
    await issue_category.find(whereStr).then(res => {
        if (res) {
            ctx.status = 200
            ctx.body = res
        } else {
            ctx.status = 404
            ctx.body = { message: "Not data" }
        }
    }).catch(error => {
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function updateIssue(ctx) {
    var whereStr = {
        id: ctx.request.body.id
    }
    var updateStr = ctx.request.body
    await issue.updateOne(whereStr, updateStr).then(res => {
        if (res.n == 1 && res.ok == 1) {
            ctx.status = 200
            ctx.body = { message: "updates Success" }
        } else {
            ctx.status = 404
            ctx.body = { message: "updates Fail" }
        }
    }).catch(error => {
        ctx.status = 500;
        ctx.body = { message: error };
        throw error;
    })
}

async function deleteIssue(ctx) {
    var whereStr = { id: ctx.query.id }
    var updateStr = { status: 2 }
    await issue.updateOne(whereStr, updateStr).then(res => {
        console.log(res)
        if (res.n == 1 && res.ok == 1) {
            ctx.status = 200
            ctx.body = { message: "Delete Success" }
        } else {
            ctx.status = 404
            ctx.body = { message: "Delete Fail" }
        }
    }).catch(error => {
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

module.exports = {
    Save,
    Find,
    findBycategory,
    updateIssue,
    deleteIssue
}