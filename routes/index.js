const router = require('koa-router')({
  prefix: ''
})

const adminUser = require('../controllers/adminusers')
const Service_Codes = require('../controllers/Service_Codes')
const Client_Keys = require('../controllers/Client_Keys')
const APP_Keys = require('../controllers/APP_Keys')
const Devices = require('../controllers/Devices')
const issue_category = require('../controllers/issue_category')
const issue = require('../controllers/issue')
const dashboard = require('../controllers/dashboard')
const user = require('../controllers/users');
const notice = require('../controllers/notice')
const areaCode = require('../controllers/areaCode');
const ota = require('../controllers/ota');
const upgrade = require('../controllers/upgrade');

//login
router.get('/adminuser', adminUser.login)
//create adminuser
router.post('/adminuser', adminUser.createAdminUser)
//find All By service_Codes （paging）
router.get('/service_Codes', Service_Codes.findByservice_Codes)
//find by By service_Codes Tables params {service_name,service_code}
// router.get('/service_Codes', Service_Codes.findByservice_Codes)
//cteated service_Codes
router.post('/service_Codes', Service_Codes.created_service_Codes)
//updated service_Codes
router.put('/service_Codes', Service_Codes.updated_service_Codes)
router.get('/findByService_code', Service_Codes.findByservice_code)
//find ALL By appclients （paging）
router.get('/client_Keys', Client_Keys.findByClient_Keys)
//find By Service_Codes && create appclients selete Service_code  
router.get('/selete_service_Codes', Client_Keys.findByservice_Codes)
//created appclients 
router.post('/client_Keys', Client_Keys.created_client_Keys)
//updated appclient
router.put('/client_Keys', Client_Keys.updated_client_Keys)
//find ALL by APP Keys （paging）
router.get('/APP_Keys', APP_Keys.findByApp_Keys)
//created APP Keys 
router.post('/APP_Keys', APP_Keys.created_APP_Keys)
//updated APP Keys
router.put('/APP_Keys', APP_Keys.updated_APP_Keys)
//find ALL by devices （paging）
router.get('/devices', Devices.findbyDevices)
//created devices
router.post('/devices', Devices.created_Devices)
//updated devices
router.put('/devices', Devices.updated_Devices)

//create issue category
router.post('/category', issue_category.create_issue_category)
router.get('/category', issue_category.findBy_issue_category)
router.get('/category_options', issue_category.findBy_issue_category_options)
router.get('/category_service_code', issue_category.findBy_issue_category_service_code)
router.delete('/category', issue_category.delete_issue_category)

router.post('/issue', issue.Save)
router.get('/issue', issue.Find)
router.get('/category_name', issue.findBycategory)
router.put('/issue', issue.updateIssue)
router.delete('/issue', issue.deleteIssue)

//查询设备数量，已经在线总数
router.get('/findByDeviceCount', dashboard.findByDeviceCount)
//查询在线设备列表
router.post('/findByOnlineDevice', dashboard.findByOnlineDevice)
//查询离线设备列表
router.post('/findByOfflineDevice', dashboard.findByOfflineDevice)
//查询FAQ数目
router.get('/findByIssueCount', dashboard.findByIssueCount)
//查询Devices 设备世界分布
router.get('/findByDeviceWorldDistribution', dashboard.findByDeviceWorldDistribution)
//查询Files
router.get('/findByFSCount', dashboard.findByFSCount)
//查询FS文件列表
router.post('/findByFSList', dashboard.findByFSList)


router.get('/users', user.findByUsers)

router.get('/usersChart', user.findByUsersChart)

router.post('/notice', notice.notice_post)

//查询国家地区编码列表
router.get('/areaCode', areaCode.findByareaCode);
//删除指定国家地区编码
router.delete('/areaCode', areaCode.deleteByareaCodeId);
//新增国家地区编码
router.post('/areaCode', areaCode.createAreaCode);

//OTA查询设备列表
router.get('/findByOTA_device', ota.findByOTA_device)
//OTA固件版本查询
router.get('/findByOTA_firmware', ota.findByOTA_firmware)
//OTA固件版本新增
router.post('/createOTA_firmware', ota.createOTA_firmware)
//OTA固件版本删除
router.delete('/delectOTA_firmware', ota.delectOTA_firmware)
//OTA固件版本信息修改
router.post('/updateOTA_firmware', ota.updateOTA_firmware)

//Dock 升级
router.post('/createByDock_Upgrade', upgrade.createByDock_Upgrade)
router.get('/findByDock_Upgrade', upgrade.findByDock_Upgrade)
router.post('/updateByDock_Upgrade', upgrade.updateByDock_Upgrade)
router.delete('/deleteByDock_Upgrade', upgrade.deleteByDock_Upgrade)

module.exports = router
