
module.exports = async function (ctx, next) {
    try {
        // console.log(ctx)

        if (ctx.method !== "OPTIONS") {
            // if (ctx.path === "/adminuser") {
            if (ctx.path === "/adminuser" || ctx.path === "/usersChart" || ctx.path === "/findByIssueCount" || ctx.path === "/findByDeviceWorldDistribution" || ctx.path === "/findByFSCount") {
                return await next();
            } else {
                if (ctx.header.authorization != undefined && ctx.header.authorization != "null" && ctx.header.authorization != "") {
                    console.log(JSON.parse(ctx.header.authorization));
                    var adminuser = require('../models/adminusers');
                    var moment = require('moment-timezone');
                    var today = moment();
                    if (JSON.parse(ctx.header.authorization).account_id != null && JSON.parse(ctx.header.authorization).account_id != "") {
                        var admin = await adminuser.find({ id: JSON.parse(ctx.header.authorization).account_id, confirmed_id: true });
                        if (admin[0]) {
                            // console.log(ctx.header.authorization.login_expired)
                            console.log(moment(admin[0].login_expired).diff(today, 'minutes'))
                            if (moment(admin[0].login_expired).diff(today, 'minutes') < 0) {
                                console.log("登录token过期")
                                ctx.status = 202;
                                ctx.body = { message: "login_token expired" };
                                return
                            } else {
                                console.log("token没过期")
                                admin[0].login_expired = moment().add(10, 'minute');
                                admin[0].save();
                            }
                        } else {
                            console.log("找不到对应adminuser");
                            ctx.status = 202;
                            ctx.body = { message: "Not adminUser" };
                            return
                        }
                    }
                } else {
                    console.log("非法请求，请求没有 auth 验证对象")
                    ctx.status = 202;
                    ctx.body = { message: "Baq Not authorization" }
                    return
                }
            }
        }
        if (ctx.accept.headers.referer.indexOf('http://172.16') < 0) {
            console.log("非法请求：" + ctx.accept.headers.referer);
            ctx.body = { code: 500, message: "Baq Request" };
            return
        }

        return await next();
    } catch (error) {
        console.log(error);
        ctx.status = 500;
        ctx.body = { message: error };
    }
}