const appclients = require('../models/appclients')

async function findByClient_KeysImpl(ctx, skips, limits) {
    var json = {}
    await appclients.countDocuments().then(res1 => {
        console.log(res1)
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await appclients.find().skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        console.log(res2)
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function findByClient_Keys_filterImpl(ctx, whereStr, skips, limits) {
    var json = {}
    await appclients.countDocuments(whereStr).then(res1 => {
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await appclients.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function findByservice_CodesImpl(ctx) {
    var servicecodes = require('../models/servicecodes')
    await servicecodes.find({}, { service_code: 1 }).then(res => {
        console.log(res);
        ctx.body = res;
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}


async function created_client_KeysImpl(ctx, insertStr) {
    var appclient = new appclients(insertStr);
    await appclient.save(insertStr).then(res => {
        console.log(res);
        ctx.body = res;
    }).catch(error => {
        console.log(error);
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function updated_client_KeysImpl(ctx, whereStr, updateStr) {
    await appclients.updateOne(whereStr, updateStr).then(res => {
        console.log(res)
        if (res.ok == 1 && res.n == 1) {
            ctx.body = { message: "succeed" }
        } else {
            ctx.body = { message: "update error !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "update error !" }
        throw error;
    })
}

module.exports = {
    findByClient_KeysImpl,
    findByservice_CodesImpl,
    created_client_KeysImpl,
    updated_client_KeysImpl,
    findByClient_Keys_filterImpl
}