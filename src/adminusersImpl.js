const adminuser = require('../models/adminusers')
const jwt = require('jsonwebtoken');
const secret = "WLINK@#@GF"
var moment = require('moment-timezone')
var random_seed = require('random-seed');

function _s4(seed_uuid) {
    var rand = random_seed.create(seed_uuid);
    return ((1 + rand.random() + Math.random()) * 0x10000 | 0).toString(16).substring(1);
}

function gen_session_key(seed_uuid) {
    return _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) +
        _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid);
}

async function loginImpl(ctx, id, Password) {
    id.confirmed_id = true
    await adminuser.find(id).then(res => {
        if (res.length >= 1) {
            console.log(jwt.verify(res[0].password, secret))
            if (Password == jwt.verify(res[0].password, secret)) {
                res[0].login_token = gen_session_key(id.id);
                res[0].login_expired = moment().add(10, 'minute');
                res[0].last_login_date = new Date;
                res[0].save();
                ctx.status = 200;
                ctx.body = { account_id: id.id, login_token: res[0].login_token, login_expired: res[0].login_expired, message: "login Success" }
            } else {
                ctx.status = 201;
                ctx.body = { message: "Password error" }
            }
        } else {
            ctx.body = { message: "ID inexistence" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function createAdminUserImpl(ctx, insertStr) {
    var adminusers = new adminuser(insertStr)
    await adminusers.save(insertStr).then(res => {
        ctx.body = res;
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

module.exports = {
    loginImpl,
    createAdminUserImpl
}