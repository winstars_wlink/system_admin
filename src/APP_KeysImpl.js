const appKey = require('../models/appKey')

async function findByApp_KeysImpl(ctx, skips, limits) {
    var json = {}
    await appKey.countDocuments().then(res1 => {
        console.log(res1)
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await appKey.find().skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        console.log(res2)
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function findByApp_Keys_filterImpl(ctx, whereStr, skips, limits) {
    var json = {}
    await appKey.countDocuments(whereStr).then(res1 => {
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await appKey.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function created_APP_KeysImpl(ctx, insertStr) {
    var app_key = new appKey(insertStr);
    await app_key.save(insertStr).then(res => {
        ctx.body = res;
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function updated_APP_KeysImpl(ctx, whereStr, updateStr) {
    await appKey.updateOne(whereStr, updateStr).then(res => {
        if (res.ok == 1 && res.n == 1) {
            ctx.body = { message: "succeed" }
        } else {
            ctx.body = { message: "update error !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "update error !" }
        throw error;
    })
}

module.exports = {
    findByApp_KeysImpl,
    findByApp_Keys_filterImpl,
    created_APP_KeysImpl,
    updated_APP_KeysImpl
}