const servicecodes = require('../models/servicecodes')

async function findByservice_CodesImpl(ctx, skips, limits) {
    var json = {}
    await servicecodes.countDocuments().then(res1 => {
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await servicecodes.find().skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function findByservice_Codes_filterImpl(ctx, whereStr, skips, limits) {
    var json = {}
    await servicecodes.countDocuments(whereStr).then(res1 => {
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await servicecodes.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function created_service_CodesImpl(ctx, insertStr) {
    var servicecode = new servicecodes(insertStr);
    await servicecode.save(insertStr).then(res => {
        ctx.body = res
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "Save error !" }
        throw error;
    })
}

async function updated_service_CodesImpl(ctx, whereStr, updateStr) {
    await servicecodes.updateOne(whereStr, updateStr).then(res => {
        console.log(res)
        if (res.ok == 1 && res.n == 1) {
            ctx.body = { message: "succeed" }
        } else {
            ctx.body = { message: "update error !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: "update error !" }
        throw error;
    })
}

async function findByservice_codeImpl(ctx) {
    await servicecodes.find().distinct("service_code").then(res => {
        ctx.status = 200;
        ctx.body = res;
    })
}

module.exports = {
    findByservice_CodesImpl,
    findByservice_Codes_filterImpl,
    created_service_CodesImpl,
    updated_service_CodesImpl,
    findByservice_codeImpl
}