const devices = require('../models/devices')

async function findbyDevicesImpl(ctx, skips, limits) {
    var json = {}
    await devices.countDocuments().then(res1 => {
        console.log(res1)
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await devices.find().skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function findbyDevices_filterImpl(ctx, whereStr, skips, limits) {
    var json = {}
    await devices.countDocuments(whereStr).then(res1 => {
        console.log(res1)
        if (res1 >= 1) {
            json.count = res1
        } else {
            ctx.body = { message: "not data" }
            return
        }
    })
    await devices.find(whereStr).skip(parseInt(skips)).limit(parseInt(limits)).then(res2 => {
        json.datas = res2
        ctx.body = json
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function created_DevicesImpl(ctx, insertStr) {
    var device = new devices(insertStr);
    await device.save(insertStr).then(res => {
        ctx.body = res;
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
}

async function updated_DevicesImpl(ctx, whereStr, updateStr) {
    await devices.updateOne(whereStr, updateStr).then(res => {
        if (res.ok == 1 && res.n == 1) {
            ctx.body = { message: "succeed" }
        } else {
            ctx.body = { message: "update error !" }
        }
    }).catch(error => {
        console.log(error)
        ctx.status = 500
        ctx.body = { message: error }
        throw error;
    })
} 

module.exports = {
    findbyDevicesImpl,
    findbyDevices_filterImpl,
    created_DevicesImpl,
    updated_DevicesImpl
}